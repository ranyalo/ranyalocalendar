<!DOCTYPE html>
<html>
   <head>
      <title>My FullCalendar</title>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.2/main.css" integrity="sha256-jLWPhwkAHq1rpueZOKALBno3eKP3m4IMB131kGhAlRQ=" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/moment@2/min/moment.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/jquery@3/dist/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.2/main.js"></script>
    </head>
    <body>
      <div class="modal" id = "eventsModal" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Ajouter un événement</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <label for="title">Titre de événement</label>
              <input type="text" class = "form-control" id = "title"><br>
              <label for="start_date">Début de l'événement</label>
              <input type="time" class = "form-control" id = "start_date"><br>
              <label for="end_date">Fin de l'événement</label>
              <input type="time" class = "form-control" id = "end_date">
              <span id = "titleError" class = "text-danger"></span>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
              <button type="button" id = "saveBtn" class="btn btn-primary">Enregistrer</button>
            </div>
          </div>
        </div>
      </div>


    <div class="modal" id = "eventsUpdate" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Modifier un événement</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <label for="titleU">Titre de événement</label>
            <input type="text" class = "form-control" id = "titleU"><br>
            <label for="start_dateU">Début de l'événement</label>
            <input type="datetime-local" class = "form-control" id = "start_dateU"><br>
            <label for="end_dateU">Fin de l'événement</label>
            <input type="datetime-local" class = "form-control" id = "end_dateU">
            <span id = "titleErrorU" class = "text-danger"></span>
          </div>
          <div class="modal-footer">
            <button type="button" id = "deleteBtnU" class="btn btn-primary">Suprimer</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
            <button type="button" id = "saveBtnU" class="btn btn-primary">Enregistrer</button>
          </div>
        </div>
      </div>
    </div>


  <div class = 'container'>
          <div class="container text-center">
             <h1>Mon calendrier avec Laravel 8</h1>
          </div>
          <div id='calendar'></div>
  </div>
      <script>

        document.addEventListener('DOMContentLoaded', function() {
          $.ajaxSetup({
              headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
            
          var rdv = @json($rdvs);
          var calendarEl = document.getElementById('calendar');
          var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
              left: 'prev,next today',
              center: 'title',
              right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            initialView: 'dayGridMonth',
            locale: 'fr',
            googleCalendarApiKey: 'AIzaSyBg0sNVmrFSHkHVXO9mjRonkdyAiEI4PWA',
            eventSources: {googleCalendarId:'jajd2g7qmgfbutbqrkcs2l5gu4@group.calendar.google.com',
                          color: 'red',
                          },
            events: rdv,
            selectable: 'true',
            editable: 'true',

            select: function(info) {
              $('#eventsModal').modal('toggle');
              $('#saveBtn').click(function() {
                var title = $('#title').val();
                var start_date = info.startStr +" "+ $('#start_date').val();
                var end_date = info.startStr +" "+ $('#end_date').val();

                $.ajax({
                     url: "{{ route('storeFun') }}",
                     type: 'POST',
                     dataType: 'json',
                     data: {title, start_date, end_date},

                     success: function(response) {
                      $('#eventsModal').modal('hide');
                      location.reload();
                     },
                     error: function(error) {
                      if (error) {
                        $('#titleError').html("</p> Veuillez ecrire un titre <p>");
                      }
                     }
                    });
              });
            },
            eventClick: function(eventObj) {
                var id = eventObj.event.id;
                var title = eventObj.event.title;
                var start_date = moment(eventObj.event.start).format('YYYY-MM-DDTHH:mm');
                var end_date = moment(eventObj.event.end).format('YYYY-MM-DDTHH:mm');
                $('#eventsUpdate').modal('toggle');
                $('#titleU').val(title);
                $('#start_dateU').val(start_date);
                $('#end_dateU').val(end_date);
                $('#saveBtnU').click(function() {
                  var title = $('#titleU').val();
                  var start_date = $('#start_dateU').val();
                  var end_date = $('#end_dateU').val();

                  $.ajax({
                      url: "{{ route('updateFun', '') }}" + '/' + id,
                      type: 'PATCH',
                      dataType: 'json',
                      data: {title, start_date, end_date},
                      success: function(response) {    
                        $('#eventsUpdate').modal('hide');
                        location.reload();
                      },
                      error: function(error) {
                      
                      }
                      });
                  });

                  $('#deleteBtnU').click(function() {
                    if(confirm('Voulez-vous vraiment supprimer l\'événement?')){
                      $.ajax({
                          url: "{{ route('destroyFun', '') }}" + '/' + id,
                          type: 'DELETE',
                          dataType: 'json',
                          data: {id},
                          success: function(response) {
                            $('#eventsUpdate').modal('hide');
                            location.reload();
                         },
                          error: function(error) {
                          
                          }
                          });
                        };
                  });
              }
          });
          calendar.render();
        });
  
      </script>
         
    </body>
  </html>