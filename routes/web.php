<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FullCalendarController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [FullCalendarController::class, 'index'])->name('indexFun');
Route::post('/', [FullCalendarController::class, 'store'])->name('storeFun');
Route::patch('/{id}', [FullCalendarController::class, 'update'])->name('updateFun');
Route::delete('/{id}', [FullCalendarController::class, 'destroy'])->name('destroyFun');

// Route::get('/', function () {
// return view('welcome');
// });
